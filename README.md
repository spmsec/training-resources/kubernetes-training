# kubernetes training

This repository contains several resources that are used during Kubernetes Trainings.

You can find some cluster resources such as common manifests that were used in the tasks, or the setup of the cluster under the `cluster-resources/` directory.

## Create your own k8s cluster

This step assumes you have `kind` installed on your machine. If you still need to install it, refer to the official [kind installation docs](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)

Create an own kubernetes trainings environment on your machine with `kind`, by running the following commands in your terminal:


```bash
#!/bin/bash

# Create kind cluster
kind create cluster \
    --config <path/to/kind-config> \
    --kubeconfig <path/to/target/kubeconfig> \
    --name <cluster-name>

# Deploy nginx-ingress controller to created cluster
kubectl apply \
    --kubeconfig <path/to/target/kubeconfig> \
    -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```

## Slides

You can find the slides for the two different trainings here:

- 2023/10: [`slides/2023-10/`](./slides/2023-10/full_slides.pdf)
- 2024/02: [`slides/2024-02/`](./slides/2024-02/full_slides.pdf) 

> the attached links target the full-slide deck of the respective training.
